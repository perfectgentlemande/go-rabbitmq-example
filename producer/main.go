package main

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"log"
	"time"
)

type AddTask struct {
	Number1 int
	Number2 int
}

func main()  {
	conn, err := amqp.Dial("amqp://localhost:55020")
	if err != nil {
		log.Fatal("cannot create connection", err)
	}
	defer conn.Close()

	amqpChannel, err := conn.Channel()
	if err != nil {
		log.Fatal("cannot create amqp channel", err)
	}
	defer amqpChannel.Close()

	addTask := AddTask{
		Number1: 12,
		Number2: 59,
	}

	data, err := json.Marshal(addTask)
	if err != nil {
		log.Fatal("cannot marshal json", err)
	}

	err = amqpChannel.Publish("", "add", false, false, amqp.Publishing{
		Headers:         nil,
		ContentType:     "application/json",
		ContentEncoding: "",
		DeliveryMode:    amqp.Persistent,
		Priority:        0,
		CorrelationId:   "",
		ReplyTo:         "",
		Expiration:      "",
		MessageId:       "",
		Timestamp:       time.Time{},
		Type:            "",
		UserId:          "",
		AppId:           "",
		Body:            data,
	})
	if err != nil {
		log.Fatal("cannot publish message", err)
	}
}