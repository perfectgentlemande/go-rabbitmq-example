package main

import (
	"encoding/json"
	"github.com/streadway/amqp"
	"log"
	"os"
	"os/signal"
	"syscall"
)

type AddTask struct {
	Number1 int
	Number2 int
}

func main() {
	conn, err := amqp.Dial("amqp://localhost:55020")
	if err != nil {
		log.Fatal("cannot create connection", err)
	}
	defer conn.Close()

	amqpChannel, err := conn.Channel()
	if err != nil {
		log.Fatal("cannot create amqp channel", err)
	}
	defer amqpChannel.Close()

	//_, err = amqpChannel.QueueDeclare("add", true, false, false, false,nil)
	//if err != nil {
	//	log.Fatal("cannot declare queue", err)
	//}

	messageChan, err := amqpChannel.Consume(
		"add",
		"",
		false,
		false,
		false,
		false,
		nil)
	if err != nil {
		log.Fatal("cannot create message chan", err)
	}

	stop := make(chan os.Signal, 4)
	signal.Notify(stop,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)


	go func() {
		log.Printf("Consumer ready, PID: %d", os.Getpid())
		for d := range messageChan {
			log.Printf("Received a message: %s", d.Body)

			addTask := &AddTask{}

			err := json.Unmarshal(d.Body, addTask)

			if err != nil {
				log.Printf("Error decoding JSON: %s", err)
			}

			log.Printf("Result of %d + %d is : %d", addTask.Number1, addTask.Number2, addTask.Number1+addTask.Number2)

			if err := d.Ack(false); err != nil {
				log.Printf("Error acknowledging message : %s", err)
			} else {
				log.Printf("Acknowledged message")
			}

		}
	}()

	<-stop
	log.Println("caught stop signal")
}
